// Теорія
// 1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Відповідь:
// - коли нам потрбіно вивести в консоль для котролю роботи коду нашу помилку без припинення роботи коду (як в практичному завданні). Тобто, коли це не помилка в JS, а те, що ми такою вважаємо;
// - коли треба забезпечити роботу всього коду без того, щоб він зупинився через помилку, яка є в окремому його блоці;
// - в роботі з сервером, коли можуть виникати певні проблеми / затримки, пов'язані безпосередньо з роботою серверу чи запитами користувачів.
// 

// Практика
// Дано масив books. Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript). На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic). Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті. Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  },
  // {
  //  author: "Гайленд"
  // },
  // {
  //  name: "Коти"
  // },
  // {
  //  price: 140
  // }
];

function getList(arr) {
  try {
    for (let i of arr) {
      if (i.hasOwnProperty("author") === false || i.hasOwnProperty("name") === false || i.hasOwnProperty("price") === false) {
        console.log(`****************`);
        throw new Error('Є помилка! У наступних картках неповні дані:');
      }
    }
  }
  
  catch (err) {
    console.log(err.message);
  }
  
  finally {
    let block = document.querySelector(".root");
    let list = document.createElement("ul");
    block.prepend(list);
    const fullList = [];

    const revArray = [];
    for (let i of arr) {
      revArray.unshift(i);
    }

    for (let i of revArray) {
      if (i.hasOwnProperty("author") === false || i.hasOwnProperty("name") === false || i.hasOwnProperty("price") === false) {

        if (!i.hasOwnProperty("name") && i.hasOwnProperty("price") && i.hasOwnProperty("author")) {
          console.log(`- у картки автора ${i.author} з ціною ${i.price} грн. не зазначено назву книги`);
        } else if (!i.hasOwnProperty("price") && i.hasOwnProperty("name") && i.hasOwnProperty("author")) {
          console.log(`- у картки автора ${i.author} не вказана ціна книги "${i.name}"`);
        } else if (!i.hasOwnProperty("author") && i.hasOwnProperty("price") && i.hasOwnProperty("name")) {
          console.log(`- у картки книги з назвою "${i.name}" та ціною ${i.price} грн. невідомий автор`);
        } else if (!i.hasOwnProperty("name") && !i.hasOwnProperty("price") && i.hasOwnProperty("author")) {
          console.log(`- у картки автора ${i.author} не зазначено назву та ціну книги`);
        } else if (!i.hasOwnProperty("name") && !i.hasOwnProperty("author") && i.hasOwnProperty("price")) {
          console.log(`- у картки з ціною ${i.price} грн. невідомі автор та назва`);
        } else if (!i.hasOwnProperty("author") && !i.hasOwnProperty("price") && i.hasOwnProperty("name")) {
          console.log(`- у картки книги з назвою "${i.name}" невідомі автор та ціна`);
        }
      }
      
      else if (i.hasOwnProperty("author") === true || i.hasOwnProperty("name") === true || i.hasOwnProperty("price") === true)
      {
        fullList.push(i);
        let listItem = document.createElement("li");
        listItem.innerHTML = `Автор: ${i.author}, книга: "${i.name}", ціна: ${i.price} грн.`;
        list.prepend(listItem);
      }
    }
    console.log(`****************`);
    console.log(`Повні картки кількістю ${fullList.length} шт. розміщені на сторінці`);
  }
}

getList(books);