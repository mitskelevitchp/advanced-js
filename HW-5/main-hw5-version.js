let block = document.querySelector(".root");
let addButton = document.querySelector(".header");

// not important part
let icon = document.createElement("img");
icon.src = "./logo.png";
icon.classList.add("icon");
block.after(icon);

let loader = document.createElement("div");
loader.classList.add("loading");
block.after(loader);

async function getUsers() {
  const persons = [];

  let response = await fetch("https://ajax.test-danit.com/api/json/users");
  let allPersonData = await response.json();

  allPersonData.forEach((i) => {
    const Cards = function (name, email, id) {
      this.id = id;
      this.name = name;
      this.email = email;
    };
    const card = new Cards(i.name, i.email, i.id);
    persons.push(card);
  });
  return persons;
}

async function getPosts() {
  let users = await getUsers();
  let mainPosts = await fetch(
    "https://ajax.test-danit.com/api/json/posts"
  ).then((response) => response.json());

  for (let i of mainPosts) {
    for (let k of users) {
      if (i.userId === k.id) {
        i.name = k.name;
        i.email = k.email;
      }
    }
  }

  const anotherPosts = shuffle(mainPosts);
  return anotherPosts;
}

// честно украдено
function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
}

async function showPosts() {
  const result = await getPosts();

  result.forEach((i) => {
    let card = document.createElement("div");
    card.classList.add("card");

    let nameWrapper = document.createElement("div");
    nameWrapper.classList.add("name-wrapper");
    card.append(nameWrapper);

    let nameWrapperText = document.createElement("div");
    nameWrapperText.classList.add("name-wrapper__text");
    nameWrapper.append(nameWrapperText);

    let name = document.createElement("a");
    name.href = "#";
    name.innerHTML = i.name;
    nameWrapperText.append(name);

    let email = document.createElement("a");
    email.href = "#";
    email.innerHTML = i.email;
    name.after(email);

    let title = document.createElement("h1");
    title.innerHTML = i.title;
    nameWrapper.after(title);

    let text = document.createElement("p");
    text.innerHTML = i.body;
    title.after(text);

    let cardBtn = document.createElement("div");
    cardBtn.classList.add("button");
    cardBtn.setAttribute("id", `${i.id}`);
    nameWrapper.append(cardBtn);

    block.append(card);
  });

  let allButtons = [...document.querySelectorAll(".button")];
  result.forEach((i) => {
    allButtons.forEach((button) => {
      button.addEventListener("click", function (e) {
        if (button.getAttribute("id") === i.id.toString()) {
          fetch(`https://ajax.test-danit.com/api/json/posts/${i.id}`, {
            method: "DELETE",
          }).then((response) => {
            if (response.ok === true) {
              console.log(`Status: ${response.status}`);
              let card = e.target.closest(".card");
              if (card) {
                card.remove();
              }
            }
          });
        }
      });
    });
  });
}

setTimeout(function () {
  icon.classList.add("none");

  setTimeout(async function () {
    loader.classList.add("none");
    await showPosts();
  }, 1000);
}, 1000);
