// Теорія.
// Технології AJAX дозволяють працювати з даними, які отримуються з серверу чи надсилаються до серверу через асинхронні запити. Такі дані легко конвертуються в формат JSON, XML чи інші для подальшої роботи з даними, а також у зворотньому напрямку - з цих форматів у код. Використання методів AJAX безпосередньо на веб-сторінці дозволяє працювати зі сторінкою (даними, які можна через неї отримати від сервера) без її перезавантаження. В той час як виконується асинхронний запит з використанням API, користувач може взаємодіяти з контентом сторінки - остання не блокується (як було свого часу).

let rootBlock = document.querySelector(".root");
// not important
let mainHeadline = document.createElement("h1");
mainHeadline.textContent = "Star Wars";
mainHeadline.classList.add("mainHeadline");
rootBlock.append(mainHeadline);
let image = document.createElement("div");
image.classList.add("image");
mainHeadline.after(image);

//
//
// Create headlines --------------------------------
async function getHeader(param1, param2 = "") {
  let headlines = [];

  await fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((res) => res.json())
    .then((data) => {
      data.forEach((elem) => {
        headlines.push(`Епізод ${elem[param1]}. ${elem[param2]}`);
      });
    });

  return headlines;
}

//
// Create synopsises --------------------------------
async function getContent(param) {
  let synopsises = [];

  await fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((res) => res.json())
    .then((data) => {
      data.forEach((elem) => {
        synopsises.push(`${elem[param]}`);
      });
    });

  return synopsises;
}

//
// Create characters --------------------------------
async function getCharacters() {
  let characters = [];

  let response = await fetch("https://ajax.test-danit.com/api/swapi/films");
  let data = await response.json();
  data.forEach((elem) => {
    characters.push(elem.characters);
  });

  // pattern for getting names of all characters
  async function getAllCharactersData(url) {
    let response = await fetch(url);
    let item = await response.json();
    let arr = [];
    arr.push(
      `${item.name}. Characteristics: gender - ${item.gender}, skin color - ${item.skinColor}, eyes color - ${item.eyeColor}, mass - ${item.mass} kg.`
    );
    return arr;
  }

  const allCharactersData = [];
  for (let i = 0; i < characters.length; i++) {
    const characterNames = [];
    for (let char of characters[i]) {
      let name = await getAllCharactersData(char);
      characterNames.push(name);
    }
    allCharactersData.push(characterNames);
  }

  return allCharactersData;
}

//
// Build the page --------------------------------
async function showData() {
  let headlinesIn = await getHeader("episodeId", "name");
  let synopsisesIn = await getContent("openingCrawl");

  const headlines = headlinesIn.reverse();
  const synopsises = synopsisesIn.reverse();

  // cards for episods
  for (let i = 0; i < headlines.length; i++) {
    let listWrapper = document.createElement("div");
    listWrapper.classList.add("episod-card");
    let headline = document.createElement("h2");
    let synopsis = document.createElement("p");
    synopsis.classList.add("par");

    headline.innerHTML = `${headlines[i]}`;
    synopsis.innerHTML = `${synopsises[i]}`;

    listWrapper.append(headline);
    listWrapper.append(synopsis);
    rootBlock.append(listWrapper);

    let episodCards = document.querySelectorAll(".episod-card");
    let loadWrapper = document.createElement("div");

    // create loader
    let load = document.createElement("div");
    let loadPar = document.createElement("p");
    loadWrapper.classList.add("loading");
    loadPar.classList.add("load-par");
    loadPar.textContent = "Content is loading...";
    loadWrapper.append(loadPar);
    load.classList.add("load");
    loadWrapper.prepend(load);
    episodCards[i].querySelector("h2").after(loadWrapper);
  }

  let allCharactersData = await getCharacters();

  let list = document.querySelectorAll(".episod-card");

  for (let i = 0; i < list.length; i++) {
    let listText = document.createElement("h3");
    listText.innerHTML = `Characters:`;
    let characterList = document.createElement("ol");

    allCharactersData[i].forEach((character) => {
      list[i].querySelector("h2").after(listText);
      list[i].querySelector("h3").after(characterList);
      let characterItem = document.createElement("li");
      characterItem.classList.add("classItem");
      characterItem.innerHTML = character;
      list[i].querySelector("ol").append(characterItem);

      // add loader
      let loading = document.querySelectorAll(".loading");
      let load = document.querySelectorAll(".load");
      loading[i].classList.add("none");
      load[i].classList.add("none");
    });
  }
}
showData();
