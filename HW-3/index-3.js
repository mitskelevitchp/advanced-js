// Теорія. Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна
// Деструктуризація - це підхід в JS, що дозволяє:
// - простіше оперувати з властивостями об'єктів та масивів, якщо нам потрібно отримати їх як окремі змінні, в т.ч. з можливістю змінювати їх назви / значення, встановлення значень за замовченням;
// - створювати нові об'єкти чи масиви на основі старих з новими / оновленими даними;
// - простіше працювати з функціями, якщо нам потрібно встановити для них параметри, що є властивостями об'єктів / масивів.

// Практика.
// Завдання 1.

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
function allClients(arr1, arr2) {
  const combineArray = [...new Set([...arr1, ...arr2])];
  return console.log(combineArray);
};
allClients(clients1, clients2);
// *******************************************************************************
// Завдання 2.

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];

function createCharacters(arr) {
  const charactersShortInfo = arr.map(({ name, lastName, age }) => ({
  ...{ name, lastName, age }
}));
  console.log(charactersShortInfo);
};
createCharacters(characters);
// *******************************************************************************
// Завдання 3.

// const user = {
//   name: "John",
//   years: 30
// };
// function showVar(arr) {
//   let { name, years, isAdmin = "false" } = arr;
//   alert(`${name}, ${years}, ${isAdmin}`);
// }
// showVar(user);
// *******************************************************************************
// Завдання 4.

// const satoshi2020 = {
//   name: 'Nick',
//   surname: 'Sabo',
//   age: 51,
//   country: 'Japan',
//   birth: '1979-08-21',
//   location: {
//     lat: 38.869422,
//     lng: 139.876632
//   }
// }
// const satoshi2019 = {
//   name: 'Dorian',
//   surname: 'Nakamoto',
//   age: 44,
//   hidden: true,
//   country: 'USA',
//   wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//   browser: 'Chrome'
// }
// const satoshi2018 = {
//   name: 'Satoshi',
//   surname: 'Nakamoto',
//   technology: 'Bitcoin',
//   country: 'Japan',
//   browser: 'Tor',
//   birth: '1975-04-05'
// }

// const fullInfo = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
// console.log(fullInfo);
// *******************************************************************************
// Завдання 5.

// const books = [{
//   name: 'Harry Potter',
//   author: 'J.K. Rowling'
// }, {
//   name: 'Lord of the rings',
//   author: 'J.R.R. Tolkien'
// }, {
//   name: 'The witcher',
//   author: 'Andrzej Sapkowski'
// }];

// const bookToAdd = {
//   name: 'Game of thrones',
//   author: 'George R. R. Martin'
// }

// function allBooks(newArray) {
//   const allBooks = [...books, newArray];
//   console.log(allBooks);
// }
// allBooks(bookToAdd);
// *******************************************************************************
// Завдання 6

// const employee = {
//   name: 'Vitalii',
//   surname: 'Klichko'
// }

// const newProp = {
//   age: 25,
//   salary: 5000
// };

// function addNewProp(arr, prop) {
//   const newObj = {...arr, ...prop};
//   console.log(newObj);
// };
// addNewProp(employee, newProp);
// *******************************************************************************
// Завдання 7.

// const array = ['value', () => 'showValue'];
// const [value, showValue] = array;
// alert(value);
// alert(showValue());