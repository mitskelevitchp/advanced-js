// Теоретичні питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Всі об'єкти в Javascript наслідують властивості та методи від батьківського об'єкта - як мінімум, від спільного для всіх об'єкта - Object.prototype (через ланцюжок наслідування).
// Наприклад, існує об'єкт А, який має певні властивості та меоди. Ми можемо створити його нащадка - об'єкт Б. Він наслідуватиме властивості та методи "батька" - об'єкту А (наприклад, через b.__proto__= a;). Ми також можемо створити "нащадка" об'єкту Б - об'єкт С, він наслідуватиме властивості та методи об'єкту Б, але й А також.
// Можна створити об'єкт за допомогою функції-конструктора та додати до нього бюдь-який метод чи властивість, прописавши їх у Prototype - кожен об'єкт-нащадок також матиме до цього доступ.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Через super() клас-нащадок наслідує властивості батьківського класу. В дужках super() зазначаються саме ті властивості, які наслідує "нащадок".
// Після super(), з нового рядка шляхом this.name зазначаються власні властиості класу-нащадка (що зазначені як аргументи constructor разом з аргументами пращура).
// Також ми маємо використовувати super(), коли створюємо у нащадка метод з тією ж назвою, що і метод батьківського класу, який звертається в своєму тілі до цього ж батьківського методу. Наприклад:
// ...
// say() {
// super.say();
// afterThatDoThis() {...}
// }
// ...

// Практичне завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата).
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this.name = prompt("Name:");
    this.age = prompt("Age:");
    this.salary = prompt("Salary:");
  }

  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = prompt("Lang:");
  }

  get salary() {
    return this._salary * 3;
  }
  set salary(value) {
    this._salary = value;
  }
}

let firstProgrammer = new Programmer();
// let secondProgrammer = new Programmer();
// let thirdProgrammer = new Programmer();
console.log(firstProgrammer);
// console.log(secondProgrammer);
// console.log(thirdProgrammer);
