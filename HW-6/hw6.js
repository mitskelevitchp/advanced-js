// Теорія.
// В сихронному коді всі операції виконуються в одному потоці - по черзі, зверху вниз, і кожна наступна операція чекає виконання попередньої. Тому якщо певна частина коду (напр., функція) вимагає більше часу на виконання, може виникнути тривала пауза. Такі ситуації часто були раніше - користувачі сайту мали чекати завантаження інтернет-сторінки після свого запиту. Поки сторінка завантажувалася, вони не мали доступу до її контенту. Сучасний асинхронний код дозволяє виконувати асинхронні ділянки коду у фоновому режимі, без блокування сторінок / додатків. Користувач, поки чекає відповіді від сервера, не втрачає доступу до контенту.

let button = document.querySelector(".button");
button.textContent = "Find by IP";
button.addEventListener("click", getAdress);

button.addEventListener("mouseover", changeText);
function changeText() {
  let button = document.querySelector(".button");
  button.textContent = "Are you sure?..";
}

button.addEventListener("mouseout", comeBackText);
function comeBackText() {
  let button = document.querySelector(".button");
  button.textContent = "Find by IP";
}

let isRequesting = false;

async function getIp() {
  try {
    let response = await fetch("https://api.ipify.org/?format=json");
    let data = await response.json();
    return data.ip;
  } catch (err) {
    alert(`Oops! There is an error: ${err.message}. Please, try again`);
  }
  location.reload();
}

async function getAdress() {
  let button = document.querySelector(".button");
  button.removeEventListener("mouseover", changeText);
  button.removeEventListener("mouseout", comeBackText);
  button.classList.add("none");

  if (isRequesting) return;

  isRequesting = true;

  let text = document.createElement("p");
  text.classList.add("text");
  text.textContent = "Searching...";
  button.after(text);

  try {
    let ip = await getIp();
    let response = await fetch(
      `http://ip-api.com/json/${ip}?fields=continent,country,regionName,city`,
      {
        method: "POST",
      }
    );
    let data = await response.json();

    let info = document.createElement("p");
    info.innerHTML = `We know your address: ${data.continent}, ${data.country}, ${data.regionName}, ${data.city}.`;
    info.classList.add("par");
    setTimeout(func, 3000);
    function func() {
      info.textContent = "Now we'll take all your money...";
      setTimeout(func, 2000);
      function func() {
        info.textContent = ";)";
        setTimeout(func, 2000);
        function func() {
          location.reload();
        }
      }
    }

    text.classList.add("none");
    button.after(info);
  } catch (err) {
    alert(`Oops! There is an error: ${err.message}. Please, try again`);
  } finally {
    isRequesting = false;
    button.removeEventListener("click", getAdress);
  }
}
